package com

import scala.concurrent.{Future}
import scala.util.{Success, Failure}
import scala.concurrent.ExecutionContext.Implicits.global
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import slick.jdbc.meta.MTable

class DBInit() {

    def createCountriesTable(dbH2: Database, table: TableQuery[Countries]) = {
        dbH2.run(DBIO.seq(
            (table.schema).create
        ))
    }

    def createAirportsTable(dbH2: Database, table: TableQuery[Airports]) = {
        dbH2.run(DBIO.seq(
            (table.schema).create
        ))
    }

    def createRunwaysTable(dbH2: Database, table: TableQuery[Runways]) = {
        dbH2.run(DBIO.seq(
            (table.schema).create
        ))
    }

    def CSV_content(file: String, op: Char) : List[Array[String]] = {
    io.Source.fromFile(file)
      .getLines().map(_.trim).map(x => {
          if (x.takeRight(1) == ",") {
              x.concat(" \"\"")
          } else {
              x
          }
      }).map(_.split(",")).toList.splitAt(1)._2
    }

    def insertCountry(cn: (String, String, String, String, String),
                      countries: TableQuery[Countries], db1: Database)
                      : Future[Unit] = {
        val insertActions = DBIO.seq(
                countries.map(p => (p.code, p.name, p.continent, p.link, p.keywords)) += cn
        )
        db1.run(insertActions)
    }

    def insertAirport(airp: (String, String, String, String),
                      airports: TableQuery[Airports], db1: Database)
                      : Future[Unit] = {

        val insertActions = DBIO.seq(
                airports.map(p => (p.ref, p.air_type, p.name, p.iso_cnt)) += airp
        )
        db1.run(insertActions)
    }

    def insertRunway(rnw: (String, String, String, String),
                     runways: TableQuery[Runways], db1: Database)
                     : Future[Unit] = {
        val insertActions = DBIO.seq(
            runways.map(p => (p.airport_ref, p.surface, p.le_ident, p.length)) += rnw
        )
        db1.run(insertActions)
    }

    def insertCountries(countries: TableQuery[Countries], db1: Database) = {

        val cnts : List[(String, String, String, String, String)] =
                   CSV_content("./csv-files/countries.csv", 'C')
                   .map(x => (x(1).replaceAll("\"", ""), x(2).replaceAll("\"", ""),
                        x(3).replaceAll("\"", ""), x(4).replaceAll("\"", ""),
                        x(5).replaceAll("\"", "")))

        cnts.foldLeft(List[Future[Unit]]())((acc, i) => {
            insertCountry(i, countries, db1)::acc
        })
    }

    def insertAirports(airports: TableQuery[Airports], db1: Database) = {

        val airpts : List[(String, String, String, String)] =
                     CSV_content("./csv-files/airports.csv", 'A')
                     .map(x => (x(0).replaceAll("\"", ""), x(2).replaceAll("\"", ""),
                     x(3).replaceAll("\"", ""),
                     x(8).replaceAll("\"", "")))

        airpts.foldLeft(List[Future[Unit]]())((acc, i) => {
            insertAirport(i, airports, db1)::acc
         })
    }

    def insertRunways(runways: TableQuery[Runways], db1: Database) = {
        val runws : List[(String, String, String, String)] =
                    CSV_content("./csv-files/runways.csv", 'R')
                    .map(x => (x(1).replaceAll("\"", ""), x(5).replaceAll("\"", ""),
                    x(8).replaceAll("\"", ""), x(3).replaceAll("\"", "")))
                    
        runws.foldLeft(List[Future[Unit]]())((acc, i) => {
                insertRunway(i, runways, db1)::acc
        })
    }

    def fill(dbH2: Database) = {

        try {

            val countries : TableQuery[Countries] = TableQuery[Countries]
            val airports : TableQuery[Airports] = TableQuery[Airports]
            val runways : TableQuery[Runways] = TableQuery[Runways]

            val tableCountry = dbH2.run(MTable.getTables("COUNTRIES"))
            tableCountry.onComplete { case Success(results) => {
                if(results.toList.isEmpty) {
                    createCountriesTable(dbH2, countries).onComplete { case (r) => {
                        println("Inserting Countries.. (step 1/3)")
                        val listCntFut = insertCountries(countries, dbH2)
                        val tableAirport = dbH2.run(MTable.getTables("AIRPORTS"))
                        tableAirport.map(airpRes => {
                            if(airpRes.toList.isEmpty) {
                                createAirportsTable(dbH2, airports).onComplete { case (r) => {
                                    val tableRunway = dbH2.run(MTable.getTables("RUNWAYS"))
                                    tableRunway.map(rnwRes => {
                                        if(rnwRes.toList.isEmpty) {
                                            createRunwaysTable(dbH2, runways).map { res => {
                                                println("Inserting Airports.. (step 2/3)")
                                                val listAirpFut = insertAirports(airports, dbH2)
                                                Future.sequence(listAirpFut.toSeq).onComplete{ case (c) => {
                                                            println("Inserting Runways.. (step 3/3)")
                                                    val listRnwsFut = insertRunways(runways, dbH2)
                                                    Future.sequence(listRnwsFut.toSeq).onComplete{ case (c) => {
                                                        Future.sequence(listCntFut.toSeq).onComplete{ case (c) => {
                                                            Application.printQuestion
                                                        }}
                                                    }}
                                                }}
                                                res
                                            }}
                                        }
                                    })
                                }
                            }}
                        })
                    }}
                }
            }
            case Failure(t) => {
                println("An error occured during table creation")
                sys.exit(1)
            }
        }
        } catch {
              case _ : RuntimeException => {
                  printf("A problem occured during database filling process")
                  System.exit(0)
              }
    }
}
}
