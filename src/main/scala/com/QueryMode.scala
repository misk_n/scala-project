package com

import scala.annotation.tailrec
import scala.concurrent.{Future}
import scala.util.{Success, Failure}
import scala.concurrent.ExecutionContext.Implicits.global
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._

class QueryMode {

    def getCountryCode(dbH2: Database, input: String) : Future[Seq[Country]] = {
        val countries : TableQuery[Countries] = TableQuery[Countries]
        val query = countries.filter( c => c.code.toUpperCase === input.toUpperCase
        || c.name.toUpperCase.startsWith(input.toUpperCase)).map(cnt => cnt).take(1)
        dbH2.run(query.result)
    }

    def getRunWaysFromAirports(dbH2: Database, code: String) : Future[Seq[(Airport, Option[Runway])]] = {
        val airports : TableQuery[Airports] = TableQuery[Airports]
        val runways : TableQuery[Runways] = TableQuery[Runways]

        val query = airports.filter(_.iso_cnt === code)
                            .joinLeft(runways)
                            .on(_.ref === _.airport_ref)

        dbH2.run(query.result)
    }

    def queryMode(dbH2: Database)= {
        def input_loop(db: Database) : Unit = {
            print("Enter country's name or code: ")
            val input = scala.io.StdIn.readLine()
            getCountryCode(db, input).onComplete { case Success(code) => {
                    if (code.size > 0) {
                        val country = code(0)
                        println("\n"+country.name)
                        getRunWaysFromAirports(dbH2, country.code) onComplete {
                            case Success(results) => {
                                if (results.size > 0) {
                                results.toList.groupBy(cpl => cpl._1).foreach{ case (airp, rnws)  => {
                                    println("    - "+airp.name+" : ")
                                    rnws.foreach(rnw => {
                                        if (rnw._2 == None) {
                                            println("              No runways found.")
                                        } else {
                                            val r = rnw._2.get
                                            print("              |  Runway id "+r.id)
                                            if (r.length != "") {
                                                println(" with a length of "+ r.length+" ft.")
                                            } else {
                                                println(" with no length found")
                                            }
                                        }
                                    })}}
                                } else {
                                    println("      No airports was found for "+country.name+".")
                                }
                                println("")
                                Application.printQuestion
                            }
                            case Failure(t) => {
                                println("An error occured during airport-runway request.")
                            }
                        }
                    } else {
                        println("/!\\ No country with this name/code was found.")
                        Application.printQuestion
                    }
                }
                case Failure(t) => {
                    println("An error occured during getCountryCode request.")
                }
            }
        }
        input_loop(dbH2)
    }

}
