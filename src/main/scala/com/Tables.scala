package com

import slick.lifted.{ProvenShape, ForeignKeyQuery}

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import slick.backend.DatabasePublisher
import scala.concurrent.duration._
import slick.jdbc.H2Profile.api._

case class Country(id: Int, code: String, name: String,
                   continent: String, link: String, keywords: String)
class Countries (tag: Tag)
  extends Table[Country](tag, "COUNTRIES") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def code = column[String]("CODE", O.Default(""))
    def name = column[String]("NAME", O.Default(""))
    def continent = column[String]("CONTINENT", O.Default(""))
    def link = column[String]("LINK", O.Default(""))
    def keywords = column[String]("KEYWORDS", O.Default(""))
    def * = (id, code, name, continent, link, keywords) <> (Country.tupled, Country.unapply)
}

case class Airport(id: Int, ref: String, air_type: String, name: String,
                   iso_cnt: String)
class Airports (tag: Tag)
  extends Table[Airport](tag, "AIRPORTS")
{
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def ref = column[String]("REF", O.Default(""))
    def air_type = column[String]("TYPE", O.Default(""))
    def name = column[String]("NAME", O.Default(""))
    def iso_cnt = column[String]("CODE", O.Default(""))

    def * = (id, ref, air_type, name, iso_cnt) <> (Airport.tupled, Airport.unapply)
}

case class Runway(id: Int, airport_ref: String, surface: String,
                  le_ident: String, length: String)
class Runways (tag: Tag)
  extends Table[Runway](tag, "RUNWAYS")
{
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def airport_ref = column[String]("AIR_REF", O.Default(""))
    def surface = column[String]("SURFACE", O.Default(""))
    def le_ident = column[String]("LE_IDENT", O.Default(""))
    def length = column[String]("LENGHT", O.Default(""))
    val airports = TableQuery[Airports]

    def airport = foreignKey("airport_fk", airport_ref, airports)(_.ref, onDelete = ForeignKeyAction.Cascade)

    def * = (id, airport_ref, surface, le_ident, length) <> (Runway.tupled, Runway.unapply)
}
