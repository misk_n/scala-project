package com

import scala.annotation.tailrec
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._

object Application extends App {

  val dbH2 = Database.forConfig("h2mem1")
  (new DBInit).fill(dbH2)

  def validateRange(value: Int, min: Int, max: Int) : Int = {
    if (value < min)
      throw new RuntimeException()
    if (value >= max)
      throw new RuntimeException()
    value
  }

  @tailrec def getUserInput : Int = {
      try {
          val input = validateRange(scala.io.StdIn.readInt(), 1, 4)
          input match {
              case 1 => {
                  println("\n==============   Query Mode   ==============\n")
                  (new QueryMode).queryMode(dbH2)
              }
              case 2 => {
                  println("\n==============   Report Mode   ==============\n")
                  printChoiceReport
                  (new ReportMode).reportMode(dbH2)
              }
              case _ => {}
          }
          input
      }
      catch {
          case _ : RuntimeException => {
            println("\nPlease enter 1 (Query) or 2 (Report) or 3 (Exit)\n")
            getUserInput
          }
      }
  }

  def printChoiceReport() = {
      println("Choose between 1, 2 and 3")
      println("1: Top 10 countries with highest and lowest number of airports")
      println("2: Type of runways per country")
      println("3: Top 10 most common runway latitude\n")
  }

  def printQuestion() = {
      println("\nChoose between Query or Reports")
      println("1: Query")
      println("2: Reports")
      println("3: Exit\n")
      print("Enter your choice: ")
  }

  while(getUserInput != 3) {}
  println("\n==============   Exit   ==============")
}
