package com

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import scala.annotation.tailrec

class ReportMode {

    def getMostCurrentLeIdent(dbH2: Database) = {
        val runways : TableQuery[Runways] = TableQuery[Runways]
        val query = runways.map(x => (x.le_ident, 1)).groupBy(_._1)
        .map{case (s, results) => (s -> results.length)}
        dbH2.run(query.result)
    }

    def getTypeOfRunways(dbH2: Database) = {
        val countries = TableQuery[Countries]
        val airports = TableQuery[Airports]
        val runways = TableQuery[Runways]

        val query = for {
            country <- countries
            airpts <- airports if country.code === airpts.iso_cnt
            rnws <- runways if airpts.ref === rnws.airport_ref
        } yield (country.name, airpts.id, rnws.surface)

        dbH2.run(query.result)
    }

    def get10HNLNAirports(dbH2: Database) : Future[Seq[(String, Option[Airport])]] = {
        val countries : TableQuery[Countries] = TableQuery[Countries]
        val airports : TableQuery[Airports] = TableQuery[Airports]

        val query = countries.joinLeft(airports)
                             .on(_.code === _.iso_cnt)
                             .map(x => (x._1.name, x._2))

         dbH2.run(query.result)
    }

  def reportMode(dbH2: Database) = {
      @tailrec def getUserInput : Unit = {

          val the10HNLNAirports = get10HNLNAirports(dbH2)
          val theTypesOfRunways = getTypeOfRunways(dbH2)
          val mostCurrentLeIdents = getMostCurrentLeIdent(dbH2)

          try {
              print("Enter you choice: ")
              val input = Application.validateRange(scala.io.StdIn.readInt(), 1, 4)
              println("\n")
              input match {
                  case 1 => {
                      println("------ Top 10 countries with the highest number of airports ----\n")
                      the10HNLNAirports.map( result => {
                          val list = result.toList.groupBy(_._1)
                                                          .map(cntAndAirports => {
                                                              if (cntAndAirports._2.head._2 == None) {
                                                                  (cntAndAirports._1, 0)
                                                              } else {
                                                                  (cntAndAirports._1, cntAndAirports._2.size)
                                                              }
                                                          })
                                                         .toSeq.toList

                          list.sortWith(_._2 > _._2).take(10)
                                                    .foreach{case (cnt, count) => {
                                                        println(cnt + " with "+ count + " airports")
                                                    }}
                          println("\n------ Top 10 countries with the lowest number of airports ----\n")
                          list.sortWith(_._2 < _._2).take(10)
                                                    .foreach{case (cnt, count) => {
                                                        println(cnt + " with "+ count + " airports")
                                                    }}
                                println("")
                                Application.printQuestion
                      })
                  }
                  case 2 => {
                      println("------ Type of runways per country ----\n")
                      theTypesOfRunways.map( rnws => {
                          rnws.toList.groupBy(ent => (ent._1, ent._3))
                          .map{case (s, results) => (s -> results.length)}
                          .groupBy(_._1._1)
                          .foreach{ case (country, map) => {
                              println("\n"+country+" :")
                              map.foreach{ case (tupleCntSurface, count) => {
                                  if (tupleCntSurface._2 != "") {
                                      println("      |  "+tupleCntSurface._2 + "  with count of: "+count)
                                  } else {
                                      println("      |  Undefined with count of: "+count)
                                  }
                              }}
                          }}
                          println("")
                          Application.printQuestion
                      })
                  }
                  case 3 => {
                      println("------ Top 10 most common LE_IDENT ----\n")
                      mostCurrentLeIdents.map(res => {
                          val leidents = res.toList.sortWith(_._2 > _._2).take(10)
                          leidents.foreach(x => {
                              println(x._1)
                          })
                          println("")
                          Application.printQuestion
                      })
                  }
              }
          }
          catch {
              case _ : RuntimeException => {
                println("\nPlease enter 1 or 2 or 3\n")
                getUserInput
              }
          }
      }
      getUserInput
  }
}
