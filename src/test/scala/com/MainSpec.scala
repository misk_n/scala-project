package com

import scala.annotation.tailrec
import scala.concurrent.{Future, Await}
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import org.scalatest._

class MainSpec extends UnitTest("Application") {

    "The validateRange function" should "raises an error" in {
        assertThrows[RuntimeException] {
            Application.validateRange(3, 1, 2)
      }
  }
}
