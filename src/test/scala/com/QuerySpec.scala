package com

import scala.concurrent.{Future, Await}
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import org.scalatest._
import slick.jdbc.meta.MTable

class QuerySpec extends UnitTest("QueryMode") {

    val querymode = new QueryMode

    "The result of getCountryCode" should "be an empty Seq" in {
        val res = Await.result(querymode.getCountryCode(UnitTest.dbH2, ""), Duration(2000, "millis"))
        res.size === 0
    }

    "getCountryCode with 'FR' as input" should "gives France on output" in {
        val code = Await.result(querymode.getCountryCode(UnitTest.dbH2, "FR"), Duration.Inf)
        assert(code.size != 0)
        assert(code(0).name.equals("France"))
    }

    "The result of getCountryCode with 'zzzee' as input" should "gives no Country as output" in {
        val code = Await.result(querymode.getCountryCode(UnitTest.dbH2, "zzzee"), Duration.Inf)
        assert(code.size == 0)
    }

    "The result of getRunWaysFromAirports with 'TK' as input" should "gives an empty Seq" in {
        val code = Await.result(querymode.getRunWaysFromAirports(UnitTest.dbH2, "TK"), Duration.Inf)
        assert(code.size == 0)
    }

    "The result of getCountryCode with 'zimb'" should "gives Zimbabwe" in {
        val code = Await.result(querymode.getCountryCode(UnitTest.dbH2, "zimb"), Duration.Inf)
        assert(code.size != 0)
        assert(code(0).name.equals("Zimbabwe"))
    }

}
