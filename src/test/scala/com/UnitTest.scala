package com

import scala.concurrent.{Future, Await}
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import org.scalatest._
import slick.jdbc.meta.MTable
import scala.concurrent.ExecutionContext.Implicits.global

abstract class UnitTest(toTest: String) extends FlatSpec {
    behavior of toTest
}

object UnitTest {
    val dbH2 = Database.forConfig("h2mem1")
    val dbinit = new DBInit

    val countries : TableQuery[Countries] = TableQuery[Countries]
    val airports : TableQuery[Airports] = TableQuery[Airports]
    val runways : TableQuery[Runways] = TableQuery[Runways]

    def initDatabase = {

        val tableCountry = dbH2.run(MTable.getTables("COUNTRIES"))
        val tableAirp =  dbH2.run(MTable.getTables("AIRPORTS"))
        val tableRnw =  dbH2.run(MTable.getTables("RUNWAYS"))

        try {
            Await.result(dbinit.createCountriesTable(dbH2, countries), Duration.Inf)
            Await.result(dbinit.createAirportsTable(dbH2, airports), Duration.Inf)
            Await.result(dbinit.createRunwaysTable(dbH2, runways), Duration.Inf)
        } catch {
            case _ : Throwable => {}
        }
    }

    def fill = {

        if (Await.result(dbH2.run(countries.result), Duration.Inf).size == 0) {
            Await.result(Future.sequence(dbinit.insertCountries(countries, dbH2)), Duration.Inf)
        }
        if (Await.result(dbH2.run(airports.result), Duration.Inf).size == 0) {
            Await.result(Future.sequence(dbinit.insertAirports(airports, dbH2)), Duration.Inf)
        }
        if (Await.result(dbH2.run(runways.result), Duration.Inf).size == 0) {
            Await.result(Future.sequence(dbinit.insertRunways(runways, dbH2)), Duration.Inf)
        }

    }

    initDatabase
    fill
}
