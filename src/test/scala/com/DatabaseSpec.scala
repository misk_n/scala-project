package com

import scala.concurrent.{Future, Await}
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import org.scalatest._
import slick.jdbc.meta.MTable

class DatabaseSpec extends UnitTest("DBInit") {

         "The COUNTRIES table" should "be created" in {
             val table = UnitTest.dbH2.run(MTable.getTables("COUNTRIES"))
             Await.result(table, Duration(2000, "millis")).size > 0
         }

         "The AIRPORTS table" should "be created" in {
             val table =  UnitTest.dbH2.run(MTable.getTables("AIRPORTS"))
             Await.result(table, Duration(2000, "millis")).size > 0
         }

         "The RUNWAYS table" should "be created" in {
             val table = UnitTest.dbH2.run(MTable.getTables("RUNWAYS"))
             Await.result(table, Duration(2000, "millis")).size > 0
         }
}
