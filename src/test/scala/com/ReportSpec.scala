package com

import scala.concurrent.{Future, Await}
import slick.backend.DatabasePublisher
import slick.jdbc.H2Profile.api._
import scala.concurrent.duration._
import org.scalatest._
import slick.jdbc.meta.MTable

class ReportSpec extends UnitTest("ReportMode") {

    val reportmode = new ReportMode

    "The most current le_ident" should "be H1" in {
        val res = Await.result(reportmode.getMostCurrentLeIdent(UnitTest.dbH2), Duration.Inf)
        assert(res.size != 0)
        assert(res.toList.sortWith(_._2 > _._2).take(1).head._1.equals("H1"))
    }

    "The country with the highest number of aiports" should "be United State" in {
        val res = Await.result(reportmode.get10HNLNAirports(UnitTest.dbH2), Duration.Inf)
        assert(res.size != 0)
        val list = res.toList.groupBy(_._1)
                  .map(cntAndAirports => {
                        if (cntAndAirports._2.head._2 == None) {
                            (cntAndAirports._1, 0)
                        } else {
                            (cntAndAirports._1, cntAndAirports._2.size)
                        }
                 })
                 .toSeq.toList

        assert(list.sortWith(_._2 > _._2).take(1).head._1.equals("United States"))
    }

    "One of the countries with the lowest number of airports" should "be Pitcairn" in {
        val res = Await.result(reportmode.get10HNLNAirports(UnitTest.dbH2), Duration.Inf)
        assert(res.size != 0)
        val list = res.toList.groupBy(_._1)
                  .map(cntAndAirports => {
                        if (cntAndAirports._2.head._2 == None) {
                            (cntAndAirports._1, 0)
                        } else {
                            (cntAndAirports._1, cntAndAirports._2.size)
                        }
                 })
                 .toSeq.toList

        assert(list.sortWith(_._2 < _._2).take(1).head._1.equals("Pitcairn"))
    }

}
