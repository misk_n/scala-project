lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.misk_n",
      scalaVersion := "2.12.5",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "Application",
    libraryDependencies ++= Seq(
        "com.typesafe.slick" %% "slick" % "3.2.3",
        "org.slf4j" % "slf4j-nop" % "1.6.4",
        "com.h2database" % "h2" % "1.3.175",
        "org.scalatest" %% "scalatest" % "3.0.5" % "test"
    )
  )
